package udep.ing.poo.medicapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.content.Intent;

public class Buscar_laboratorio extends AppCompatActivity implements OnClickListener{
    private Button buscarLaboratoriosPorVacuna,verRegistrosDeLaboratorios,laboratorio;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buscar_laboratorio);
        buscarLaboratoriosPorVacuna=(Button)findViewById(R.id.buttonBuscarLaboratorioPorVacuna);
        buscarLaboratoriosPorVacuna.setOnClickListener(this);
        verRegistrosDeLaboratorios=(Button)findViewById(R.id.buttonVerRegistrosDeLaboratorios);
        verRegistrosDeLaboratorios.setOnClickListener(this);
        laboratorio = (Button) findViewById(R.id.buttonLaboratorio);
        laboratorio.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if(view.getId()==buscarLaboratoriosPorVacuna.getId()) {
            Intent intent = new Intent(this, Buscar_laboratorio_por_vacuna.class);
            startActivity(intent);
        }else if (view.getId()==verRegistrosDeLaboratorios.getId()){
            Intent intent = new Intent(this, Buscar_laboratorio_registros_de_laboratorios.class);
            startActivity(intent);
        }else if (view.getId() == laboratorio.getId()) {
            Intent intent = new Intent(this, laboratorio.class);
            startActivity(intent);
        }


    }
}

