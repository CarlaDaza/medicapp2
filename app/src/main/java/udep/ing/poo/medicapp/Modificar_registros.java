package udep.ing.poo.medicapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import udep.ing.poo.medicapp.db.Conexion;
import udep.ing.poo.medicapp.db.Paciente;

public class Modificar_registros extends AppCompatActivity implements View.OnClickListener {
    private EditText nombreDelPaciente,fechaDeNacimiento,nombreDelApoderado,telefonoDelApoderado,correoDelApoderado,alergias,dni;
    private Button modificar, eliminar;
    private Conexion conexion;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modificar_registros);

        this.conexion=new Conexion(this);

        dni=(EditText)findViewById(R.id.editTextDni);
        nombreDelPaciente=(EditText)findViewById(R.id.editTextNombreDelPaciente);
        fechaDeNacimiento=(EditText)findViewById(R.id.editTextFechaDeNacimiento);
        nombreDelApoderado=(EditText)findViewById(R.id.editTextNombreDelApoderado);
        telefonoDelApoderado=(EditText)findViewById(R.id.editTextTelefonoDelApoderado);
        correoDelApoderado=(EditText)findViewById(R.id.editTextCorreoDelApoderado);
        alergias=(EditText)findViewById(R.id.editTextAlergias);

        modificar=(Button)findViewById(R.id.buttonModificar);
        modificar.setOnClickListener(this);

        eliminar=(Button)findViewById(R.id.buttonEliminar);
        eliminar.setOnClickListener(this);

        Bundle extras = getIntent().getExtras();
        int dni = extras.getInt(Paciente.CAMPO_DNI);
        String nombreDelPaciente = extras.getString(Paciente.CAMPO_NOMBRE_PACIENTE);
        String fechaDeNacimiento = extras.getString(Paciente.CAMPO_FECHA_NACIMIENTO);
        String nombreApoderado = extras.getString(Paciente.CAMPO_NOMBRE_APODERADO);
        String telefonoDelApoderado = extras.getString(Paciente.CAMPO_TELEFONO_APODERADO);
        String correoDelApoderado = extras.getString(Paciente.CAMPO_CORREO_APODERADO);
        String alergias = extras.getString(Paciente.CAMPO_ALERGIAS);

        this.dni.setText(dni+"");
        this.nombreDelPaciente.setText(nombreDelPaciente);
        this.fechaDeNacimiento.setText(fechaDeNacimiento);
        this.nombreDelApoderado.setText(nombreApoderado);
        this.telefonoDelApoderado.setText(telefonoDelApoderado);
        this.correoDelApoderado.setText(correoDelApoderado);
        this.alergias.setText(alergias);

    }

    @Override
    public void onClick(View v) {
        SQLiteDatabase db=conexion.getWritableDatabase();

        String[] dniPaciente = {this.dni.getText().toString()};

        if(v.getId()==modificar.getId()){

            ContentValues values = new ContentValues();

            values.put(Paciente.CAMPO_DNI,this.dni.getText().toString());
            values.put(Paciente.CAMPO_NOMBRE_PACIENTE,this.nombreDelPaciente.getText().toString());
            values.put(Paciente.CAMPO_FECHA_NACIMIENTO,this.fechaDeNacimiento.getText().toString());
            values.put(Paciente.CAMPO_NOMBRE_APODERADO,this.nombreDelApoderado.getText().toString());
            values.put(Paciente.CAMPO_TELEFONO_APODERADO,this.telefonoDelApoderado.getText().toString());
            values.put(Paciente.CAMPO_CORREO_APODERADO,this.correoDelApoderado.getText().toString());
            values.put(Paciente.CAMPO_ALERGIAS,this.alergias.getText().toString());

            db.update(Paciente.TABLA_PACIENTE,values,Paciente.CAMPO_DNI+"=?",dniPaciente);
            Toast.makeText(this,"Se modificó el registro correctamente",Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(this,Tablero_de_opciones.class);
            startActivity(intent);
        }
        if(v.getId()==eliminar.getId()){
            db.delete(Paciente.TABLA_PACIENTE,Paciente.CAMPO_DNI+"=?",dniPaciente);
            Toast.makeText(this,"Se eliminó el registro correctamente",Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(this,Tablero_de_opciones.class);
            startActivity(intent);

        }
    }
}