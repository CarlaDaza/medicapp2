package udep.ing.poo.medicapp.db;

public class  Visita {
    private int id;
    private String motivo, observaciones, recomendaciones;

    public Visita(int id, String motivo, String observaciones, String recomendaciones) {
        this.id = id;
        this.motivo = motivo;
        this.observaciones = observaciones;
        this.recomendaciones = recomendaciones;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public String getRecomendaciones() {
        return recomendaciones;
    }

    public void setRecomendaciones(String recomendaciones) {
        this.recomendaciones = recomendaciones;
    }
    public static final String CREAR_TABLA_VISITA = "CREATE TABLE visita (id INTEGER, motivo TEXT, observaciones TEXT, recomendaciones TEXT) ";
    public static final String DROPEAR_TABLA_VISITA = "DROP TABLE IF EXISTS visita ";

    public static final String TABLA_VISITA = "visita";
    public static final String CAMPO_ID = "id";
    public static final String CAMPO_MOTIVO = "motivo";
    public static final String CAMPO_OBSERVACIONES = "observaciones";
    public static final String CAMPO_RECOMENDACIONES = "recomendaciones";





}
