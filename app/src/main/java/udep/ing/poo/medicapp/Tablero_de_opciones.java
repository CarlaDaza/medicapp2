package udep.ing.poo.medicapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class Tablero_de_opciones extends AppCompatActivity implements OnClickListener{
    private Button registrarPaciente;
    private Button ingresarVisita;
    private Button revisarRegistros;
    private Button buscarLaboratorios;
    private Button verCalendario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tablero_de_opciones);

        registrarPaciente=(Button)findViewById(R.id.buttonRegistrarPaciente);
        registrarPaciente.setOnClickListener(this);

        ingresarVisita=(Button)findViewById(R.id.buttonIngresarVisita);
        ingresarVisita.setOnClickListener(this);

        revisarRegistros=(Button)findViewById(R.id.buttonRevisarRegistros);
        revisarRegistros.setOnClickListener(this);

        buscarLaboratorios=(Button)findViewById(R.id.buttonBuscarLaboratorio);
        buscarLaboratorios.setOnClickListener(this);

        verCalendario=(Button)findViewById(R.id.buttonVerCalendario);
        verCalendario.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        if(view.getId()==registrarPaciente.getId()){
            Intent intent=new Intent(this,Registrar_paciente.class);
            startActivity(intent);
        }else if(view.getId()==ingresarVisita.getId()){
            Intent intent=new Intent(this,Ingresar_visita.class);
            startActivity(intent);
        }else if(view.getId()==revisarRegistros.getId()){
            Intent intent=new Intent(this,Revisar_registros.class);
            startActivity(intent);
        }else if(view.getId()==buscarLaboratorios.getId()) {
            Intent intent = new Intent(this, Buscar_laboratorio.class);
            startActivity(intent);
        }else if(view.getId()==verCalendario.getId()) {
            Intent intent = new Intent(this, Calendario.class);
            startActivity(intent);
        }

    }
}