package udep.ing.poo.medicapp.db;

public class Paciente {
    private int dni;
    private String telefonoApoderado;
    private String nombrePaciente, nombreApoderado, correoApoderado, alergias;
    private String fechaNacimiento;

    public Paciente(int dni,  String nombrePaciente, String fechaNacimiento, String nombreApoderado, String telefonoApoderado,String correoApoderado, String alergias) {
        this.dni = dni;
        this.telefonoApoderado = telefonoApoderado;
        this.nombrePaciente = nombrePaciente;
        this.nombreApoderado = nombreApoderado;
        this.correoApoderado = correoApoderado;
        this.alergias = alergias;
        this.fechaNacimiento = fechaNacimiento;
    }

    public int getDni() {
        return dni;
    }

    public void setDni(int dni) {
        this.dni = dni;
    }

    public String getTelefonoApoderado() {
        return telefonoApoderado;
    }

    public void setTelefonoApoderado(String telefonoApoderado) {
        this.telefonoApoderado = telefonoApoderado;
    }

    public String getNombrePaciente() {
        return nombrePaciente;
    }

    public void setNombrePaciente(String nombrePaciente) {
        this.nombrePaciente = nombrePaciente;
    }

    public String getNombreApoderado() {
        return nombreApoderado;
    }

    public void setNombreApoderado(String nombreApoderado) {
        this.nombreApoderado = nombreApoderado;
    }

    public String getCorreoApoderado() {
        return correoApoderado;
    }

    public void setCorreoApoderado(String correoApoderado) {
        this.correoApoderado = correoApoderado;
    }

    public String getAlergias() {
        return alergias;
    }

    public void setAlergias(String alergias) {
        this.alergias = alergias;
    }

    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public static final String CREAR_TABLA_PACIENTE = "CREATE TABLE paciente (dni INTEGER, nombrePaciente TEXT, fechaNacimiento TEXT, nombreApoderado TEXT, telefonoApoderado TEXT, correoApoderado TEXT, alergias TEXT) ";
    public static final String DROPEAR_TABLA_PACIENTE = "DROP TABLE IF EXISTS paciente ";

    public static final String TABLA_PACIENTE = "paciente";
    public static final String CAMPO_DNI = "dni";
    public static final String CAMPO_NOMBRE_PACIENTE = "nombrePaciente";
    public static final String CAMPO_TELEFONO_APODERADO = "telefonoApoderado";
    public static final String CAMPO_FECHA_NACIMIENTO = "fechaNacimiento";
    public static final String CAMPO_NOMBRE_APODERADO = "nombreApoderado";
    public static final String CAMPO_CORREO_APODERADO = "correoApoderado";
    public static final String CAMPO_ALERGIAS = "alergias";


}
