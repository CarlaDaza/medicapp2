package udep.ing.poo.medicapp;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import udep.ing.poo.medicapp.db.Conexion;
import udep.ing.poo.medicapp.db.Paciente;

public class Registrar_paciente extends AppCompatActivity implements OnClickListener {

    private EditText nombreDelPaciente,fechaDeNacimiento,nombreDelApoderado,telefonoDelApoderado,correoDelApoderado,alergias,dni;
    private Button guardar;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrar_paciente);

        nombreDelPaciente=(EditText)findViewById(R.id.editTextNombreDelPaciente);
        fechaDeNacimiento=(EditText)findViewById(R.id.editTextFechaDeNacimiento);
        nombreDelApoderado=(EditText)findViewById(R.id.editTextNombreDelApoderado);
        telefonoDelApoderado=(EditText)findViewById(R.id.editTextTelefonoDelApoderado);
        correoDelApoderado=(EditText)findViewById(R.id.editTextCorreoDelApoderado);
        dni=(EditText)findViewById(R.id.editTextDni);
        alergias=(EditText)findViewById(R.id.editTextAlergias);

        guardar=(Button)findViewById(R.id.buttonEliminar);
        guardar.setOnClickListener(this);
    }
    




    public void onClick(View v){


        if (v.getId()==guardar.getId()){
            if (nombreDelPaciente.getText().toString().equals("")||dni.getText().toString().equals("") || fechaDeNacimiento.getText().toString().equals("") || nombreDelApoderado.getText().toString().equals("") || telefonoDelApoderado.getText().toString().equals("") || correoDelApoderado.getText().toString().equals("") || alergias.getText().toString().equals("")){
                Toast.makeText(getApplicationContext(),"Por favor llene todos los campos",Toast.LENGTH_SHORT).show();
            }else{
                Conexion conexion = new Conexion(this);
                SQLiteDatabase db = conexion.getWritableDatabase();

                ContentValues values = new ContentValues();

                values.put(Paciente.CAMPO_DNI, dni.getText().toString());
                values.put(Paciente.CAMPO_NOMBRE_PACIENTE, nombreDelPaciente.getText().toString());
                values.put(Paciente.CAMPO_FECHA_NACIMIENTO,fechaDeNacimiento.getText().toString());
                values.put(Paciente.CAMPO_NOMBRE_APODERADO,nombreDelApoderado.getText().toString());
                values.put(Paciente.CAMPO_TELEFONO_APODERADO, telefonoDelApoderado.getText().toString());
                values.put(Paciente.CAMPO_CORREO_APODERADO,correoDelApoderado.getText().toString());
                values.put(Paciente.CAMPO_ALERGIAS,alergias.getText().toString());


                db.insert(Paciente.TABLA_PACIENTE, Paciente.CAMPO_DNI, values);

                db.close();


                Intent intent = new Intent(this, Tablero_de_opciones.class);
                startActivity(intent);
                Toast.makeText(getApplicationContext(),"Paciente registrado con éxito",Toast.LENGTH_SHORT).show();
            }
        }
    }


}


