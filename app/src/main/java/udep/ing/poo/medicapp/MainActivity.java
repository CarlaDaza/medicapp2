package udep.ing.poo.medicapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.content.Intent;

public class MainActivity extends AppCompatActivity implements OnClickListener {
    private Button comenzar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        comenzar=(Button)findViewById(R.id.buttonComenzar);
        comenzar.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if(view.getId()==comenzar.getId()){
            Intent intent=new Intent(this,Tablero_de_opciones.class);
            startActivity(intent);
        }
    }

    }

