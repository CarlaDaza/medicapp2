package udep.ing.poo.medicapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class Buscar_laboratorio_por_vacuna extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    private Spinner spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buscar_laboratorio_por_vacuna);
        spinner=(Spinner)findViewById(R.id.spinner);

        List<String> items = new ArrayList<String>();
        items.add("Seleccione una vacuna");
        items.add("Avaxim Ped");
        items.add("Fluquadri Ped");
        items.add("Havrix");
        items.add("Hexaxim");
        items.add("Infanrix Hexa");
        items.add("Infanrix IPV");
        items.add("Pentaxim");
        items.add("Prevenar 13");
        items.add("Proquad");
        items.add("Rotateq");

        ArrayAdapter<String> adapter=new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,items);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);

    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
        if ((adapterView.getItemAtPosition(position).toString()).equals("Avaxim Ped")){
            Intent intent=new Intent(this,Laboratorio_Sanofi.class);
            startActivity(intent);
        }else if((adapterView.getItemAtPosition(position).toString()).equals("Fluquadri Ped")){
            Intent intent=new Intent(this,Laboratorio_Sanofi.class);
            startActivity(intent);
        }else if((adapterView.getItemAtPosition(position).toString()).equals("Pentaxim")) {
            Intent intent = new Intent(this, Laboratorio_Sanofi.class);
            startActivity(intent);
        }else if((adapterView.getItemAtPosition(position).toString()).equals("Hexaxim")) {
            Intent intent = new Intent(this, Laboratorio_Sanofi.class);
            startActivity(intent);
        }else if((adapterView.getItemAtPosition(position).toString()).equals("Infanrix Hexa")) {
            Intent intent = new Intent(this, Laboratorio_Glaxo.class);
            startActivity(intent);
        }else if((adapterView.getItemAtPosition(position).toString()).equals("Infanrix IPV")) {
            Intent intent = new Intent(this, Laboratorio_Glaxo.class);
            startActivity(intent);
        }else if((adapterView.getItemAtPosition(position).toString()).equals("Havrix")) {
            Intent intent = new Intent(this, Laboratorio_Glaxo.class);
            startActivity(intent);
        }else if((adapterView.getItemAtPosition(position).toString()).equals("Rotateq")) {
            Intent intent = new Intent(this, Laboratorio_Msd.class);
            startActivity(intent);
        }else if((adapterView.getItemAtPosition(position).toString()).equals("Proquad")) {
            Intent intent = new Intent(this, Laboratorio_Msd.class);
            startActivity(intent);
        }else if((adapterView.getItemAtPosition(position).toString()).equals("Prevenar 13")) {
            Intent intent = new Intent(this, Laboratorio_boticas_y_salud.class);
            startActivity(intent);
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}