package udep.ing.poo.medicapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class Buscar_laboratorio_registros_de_laboratorios extends AppCompatActivity implements AdapterView.OnItemClickListener {
    ListView listView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buscar_laboratorio_registros_de_laboratorios);
        listView = (ListView) findViewById(R.id.listViewLaboratorios);

        List<String> items = new ArrayList<String>();
        items.add("Laboratorio SANOFI");
        items.add("Laboratorio Glaxo Smith Kline");
        items.add("Laboratorio MSD");
        items.add("Boticas y Salud");

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, items);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);

    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if ((parent.getItemAtPosition(position).toString()).equals("Laboratorio SANOFI")) {
            Intent intent = new Intent(this, Laboratorio_Sanofi.class);
            startActivity(intent);
        }else if ((parent.getItemAtPosition(position).toString()).equals("Laboratorio Glaxo Smith Kline")) {
            Intent intent = new Intent(this, Laboratorio_Glaxo.class);
            startActivity(intent);
        }else if ((parent.getItemAtPosition(position).toString()).equals("Laboratorio MSD")) {
            Intent intent = new Intent(this, Laboratorio_Msd.class);
            startActivity(intent);
        }else if ((parent.getItemAtPosition(position).toString()).equals("Boticas y Salud")) {
            Intent intent = new Intent(this, Laboratorio_boticas_y_salud.class);
            startActivity(intent);
        }

    }
}