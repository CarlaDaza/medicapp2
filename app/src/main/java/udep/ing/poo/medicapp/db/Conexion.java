package udep.ing.poo.medicapp.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class Conexion extends SQLiteOpenHelper {

    public Conexion(@Nullable Context context) {

        super(context, BD_PACIENTE, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(Paciente.CREAR_TABLA_PACIENTE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL(Paciente.DROPEAR_TABLA_PACIENTE);
        onCreate(sqLiteDatabase);
    }

    public static final String BD_PACIENTE = "bd_paciente";
}
