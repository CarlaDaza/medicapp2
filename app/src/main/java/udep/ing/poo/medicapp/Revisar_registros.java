package udep.ing.poo.medicapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.LinkedList;
import java.util.List;

import udep.ing.poo.medicapp.db.Conexion;
import udep.ing.poo.medicapp.db.Paciente;

public class Revisar_registros extends AppCompatActivity implements AdapterView.OnItemClickListener {

    private ListView listViewRegistros;
    private Conexion conexion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_revisar_registros);

        this.conexion = new Conexion(this);

        listViewRegistros = (ListView)findViewById(R.id.listViewRegistros);

        ArrayAdapter<String> adaptador = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, getListaDePacientes());

        listViewRegistros.setAdapter(adaptador);

        listViewRegistros.setOnItemClickListener(this);
    }
    private List<Paciente> listaPacientes;

    private List<String> getListaDePacientes() {

        listaPacientes = new LinkedList<Paciente>();
        List<String> listaNombresPacientes = new LinkedList<String>();

        SQLiteDatabase db = conexion.getReadableDatabase();

        String[] campos = {Paciente.CAMPO_DNI, Paciente.CAMPO_NOMBRE_PACIENTE, Paciente.CAMPO_FECHA_NACIMIENTO,
                Paciente.CAMPO_NOMBRE_APODERADO,Paciente.CAMPO_TELEFONO_APODERADO,Paciente.CAMPO_CORREO_APODERADO,
                Paciente.CAMPO_ALERGIAS};

        Cursor cursor = db.query(Paciente.TABLA_PACIENTE, campos, null, null, null, null, null);
        while (cursor.moveToNext()) {

            int dni = cursor.getInt(0);
            String nombrePaciente = cursor.getString(1);
            String fechaNacimiento = cursor.getString(2);
            String nombreApoderado = cursor.getString(3);
            String telefonoApoderado = cursor.getString(4);
            String correoApoderado = cursor.getString(5);
            String alergias = cursor.getString(6);

            Paciente paciente = new Paciente(dni,nombrePaciente,fechaNacimiento,nombreApoderado,telefonoApoderado,correoApoderado,alergias);
            listaPacientes.add(paciente);

            listaNombresPacientes.add(paciente.getNombrePaciente());

        }

        return listaNombresPacientes;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Paciente paciente = listaPacientes.get(position);
        Intent intent = new Intent(this, Modificar_registros.class);

        Bundle extras = new Bundle();

        extras.putInt(Paciente.CAMPO_DNI,paciente.getDni());
        extras.putString(Paciente.CAMPO_NOMBRE_PACIENTE, paciente.getNombrePaciente());
        extras.putString(Paciente.CAMPO_FECHA_NACIMIENTO, paciente.getFechaNacimiento());
        extras.putString(Paciente.CAMPO_NOMBRE_APODERADO, paciente.getNombreApoderado());
        extras.putString(Paciente.CAMPO_TELEFONO_APODERADO, paciente.getTelefonoApoderado());
        extras.putString(Paciente.CAMPO_CORREO_APODERADO, paciente.getCorreoApoderado());
        extras.putString(Paciente.CAMPO_ALERGIAS, paciente.getAlergias());

        intent.putExtras(extras);

        startActivity(intent);

    }
}