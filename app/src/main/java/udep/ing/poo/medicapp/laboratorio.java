package udep.ing.poo.medicapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class laboratorio extends AppCompatActivity implements AdapterView.OnItemClickListener, TextView.OnEditorActionListener {

        ListView listView;
        EditText editTextTextPersonName;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_laboratorio);

            editTextTextPersonName = (EditText)findViewById(R.id.editTextTextPersonName);
            editTextTextPersonName.setOnEditorActionListener(this);

            SharedPreferences sp = getSharedPreferences("m2",MODE_PRIVATE);
            String nombre= sp.getString("nombre", "");
            editTextTextPersonName.setText(nombre);

            listView = (ListView)findViewById(R.id.listView);
            setListView();

        }

        private void setListView() {

            List<String> items = getItens();

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, items);
            listView.setAdapter(adapter);
            listView.setOnItemClickListener(this);

        }

        private List<String> getItens() {

            List<String> items = new ArrayList<String>();
            items.add("Laboratorio SANOFI");
            items.add("Laboratorio Glaxo Smith Kline");
            items.add("Laboratorio MSD");
            items.add("Boticas y Salud");

            String nombre = editTextTextPersonName.getText().toString();
            for(int i=0; i<items.size(); i++) {
                if(!items.get(i).contains(nombre)) {
                    items.remove(i);
                    i--;
                }

            }

            return items;

        }

        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

            //if (adapterView.getId() == listView.getId()) {

            //String seleccionado = adapterView.getItemAtPosition(i).toString();
            //Toast.makeText(getApplicationContext(), seleccionado,Toast.LENGTH_SHORT).show();


            //}
            if ((adapterView.getItemAtPosition(i).toString()).equals("Laboratorio SANOFI")) {
                Intent intent = new Intent(this, Laboratorio_Sanofi.class);
                startActivity(intent);
            }else if ((adapterView.getItemAtPosition(i).toString()).equals("Laboratorio Glaxo Smith Kline")) {
                Intent intent = new Intent(this, Laboratorio_Glaxo.class);
                startActivity(intent);
            }else if ((adapterView.getItemAtPosition(i).toString()).equals("Laboratorio MSD")) {
                Intent intent = new Intent(this, Laboratorio_Msd.class);
                startActivity(intent);
            }else if ((adapterView.getItemAtPosition(i).toString()).equals("Boticas y Salud")) {
                Intent intent = new Intent(this, Laboratorio_boticas_y_salud.class);
                startActivity(intent);
            }
        }

        @Override
        public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {

            if (textView.getId() == editTextTextPersonName.getId()) {

                String nombre = editTextTextPersonName.getText().toString();
                Toast.makeText(getApplicationContext(), nombre, Toast.LENGTH_SHORT).show();
                setListView();

                SharedPreferences.Editor editor = getSharedPreferences("m2",MODE_PRIVATE).edit();
                editor.putString("nombre", editTextTextPersonName.getText().toString());
                editor.commit();
            }
            return false;
        }
}
