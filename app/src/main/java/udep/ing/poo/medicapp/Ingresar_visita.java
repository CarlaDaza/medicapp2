package udep.ing.poo.medicapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class Ingresar_visita extends AppCompatActivity implements View.OnClickListener {
    private Button guardar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ingresar_visita);

        guardar=(Button)findViewById(R.id.buttonGuardarVisita);
        guardar.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if(view.getId()==guardar.getId()) {
            Intent intent = new Intent(this, Tablero_de_opciones.class);
            startActivity(intent);
        }
    }
}